#!/bin/bash

function mapGPIO() {
  local GPIO=${1}
  if [ ! -d /sys/class/gpio/gpio${GPIO} ]; then
    echo ${GPIO} > /sys/class/gpio/export
  fi
}

function setGPIOMode() {
  local GPIO=${1}
  local MODE=${2}
  echo ${MODE} > /sys/class/gpio/gpio${GPIO}/direction
}

function getGPIOMode() {
  local GPIO=${1}
  cat /sys/class/gpio/gpio${GPIO}/direction
}

function writeGPIO() {
  local GPIO=${1}
  local VALUE=${2}

  echo ${VALUE} > /sys/class/gpio/gpio${GPIO}/value
}

function readGPIO() {
  local GPIO=${1}
  cat /sys/class/gpio/gpio${GPIO}/value 
}

