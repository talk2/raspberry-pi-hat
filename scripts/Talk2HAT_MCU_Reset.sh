#!/bin/bash
source ./Talk2HAT_Library.sh


if [ "${1}" == "boot0" ]; then
  echo "### Restarting MCU on Boot 0 Mode ###"
  boot0=1
else
  echo "### Restarting MCU ###"
  boot0=0
fi

echo "  MCU Reset - GPIO21/PIN40: out: 0"
mapGPIO 21
setGPIOMode 21 out
writeGPIO 21 0

sleep 1

echo "  MCU Boot 0 - GPIO27/PIN13: out: ${boot0}"
mapGPIO 27
setGPIOMode 27 out
writeGPIO 27 ${boot0}

sleep 1

echo "  MCU Reset - GPIO21/PIN40: out: 1"
mapGPIO 21
setGPIOMode 21 out
writeGPIO 21 1

