#!/bin/bash
echo "If this command fails, make sure EEPROM WP GPIO is in the correct state."
echo ""

echo "### Listing I2C devices on Bus 0 ###"
i2cdetect 0
sleep 2

echo "### Configuring EEPROM 24c32 on Address 0x50 ###"
echo "24c32 0x50" > /sys/class/i2c-adapter/i2c-0/new_device
sleep 2

echo "### Writting directly to the EEPROM ###"
testStr="Test: $( date +"%c - %N")"
echo "Writting: ${testStr}"
echo ${testStr} > /sys/class/i2c-adapter/i2c-0/0-0050/eeprom

echo "### Reading directly from the EEPROM ###"
head -n 1 /sys/class/i2c-adapter/i2c-0/0-0050/eeprom

echo "### Deleting EEPROM I2C Device ###"
echo "0x50" > /sys/class/i2c-adapter/i2c-0/delete_device
