#!/bin/bash
echo "If this command fails, make sure CAN Reset GPIO is in the correct state."
echo ""
echo "### Removing mcp251x module ###"
rmmod mcp251x
sleep 1

echo "### Loading mcp251x module ###"
modprobe mcp251x
sleep 1

echo "### Configuring CAN0 interface and showing Stats ###"
ip link set can0 down
ip link set can0 up txqueuelen 1000 type can bitrate 250000 restart-ms 500
ip -details -statistics link show can0
