#!/bin/bash
source ./Talk2HAT_Library.sh

echo "### Enabling GPIOs used to interact with STM32 MCU ###"

echo "  MCU Boot 0 - GPIO27/PIN13: out: 0"
mapGPIO 27
setGPIOMode 27 out
writeGPIO 27 0

echo "  RPI CAN Reset - GPIO26/PIN37: out: 1"
mapGPIO 26
setGPIOMode 26 out
writeGPIO 26 1

echo "  MCU Reset - GPIO21/PIN40: out: 1"
mapGPIO 21
setGPIOMode 21 out
writeGPIO 21 1

echo "  EEPROM R/W - GPIO16/PIN36: out: 0"
mapGPIO 16
setGPIOMode 16 out
writeGPIO 16 0
