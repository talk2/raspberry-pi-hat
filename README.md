This repository contains a set of code, samples, libraries, scripts and documentation for the Talk² Raspberry PI HAT. For additional information about this product or about the Talk² project, please visit the official site at [http://talk2.wisen.com.au](Link URL).

# Hardware #

## Physical Connection ##
The drawing below shows how to attach the Raspberry PI and the Talk² RPI HAT together. The easiest way to assemble is to first attach the Bottom Screw, Washer and the Spacer Nut to the RPI and later later place the HAT on the top, locking it in place with the Top Screw.

There's no need to use all four mounting holes, everything will be very secure with only two screws, leaving the other two mounting holes free to fix the board to a case for example.
```
#!
                                    ┌══════┐
                                    └═╤══╤═┘
                                      │≈≈│
                 Nylon Top Screw -->  │≈≈│
                                      │≈≈│
                                      └══┘             ╔═╤╤═╤╤═╤╤═╤╤═╤╤═╤═  <-- Talk² RPI HAT SMT Header
                       ┌──────────────    ─────────────╙─╫╫─╫╫─╫╫─╫╫─╫╫─╫─
    Talk² RPI HAT -->  └──────────────    ────────────────────────────────

                                    ╓═----═╖             || || || || || |
                                    ║ ≈  ≈ ║           ╔═╧╧═╧╧═╧╧═╧╧═╧╧═╧═
      Nylon Spacer Screw Nut -->    ║ ≈  ≈ ║           ║                    <-- Extension Header
                                    ║ ≈  ≈ ║           ╙─╨╨─╨╨─╨╨─╨╨─╨╨─╨─
                                    ║ ≈  ≈ ║
                                    ╙═----═╜             || || || || || |  
                                                       ╔═╧╧═╧╧═╧╧═╧╧═╧╧═╧═
          Nylon Screw Washer -->  ▓▓▓▓    ▓▓▓▓         ║                    <-- RPI Header
                       ┌──────────────    ─────────────╙──────────────────
     Raspberry PI -->  └──────────────    ────────────────────────────────
                                      ┌══┐
                                      │≈≈│
              Nylon Bottom Screw -->  │≈≈│
                                      │≈≈│
                                    ┌═╧══╧═┐
                                    └══════┘
```
## Board Layout ##
The layout below shows the position for the most important connectors and components.
```
#!
                                              ╓───╖
                                             ┌╢   ╟┐
                    ╔════════════════════════╡     ╞════════════════════════╗
                    ║     *┌───────────┐     └─────┘    *┌───────────┐      ║
                    ║  ()  └───────────┘       CN3       └───────────┘  ()  ║
                    ║               PAD1               ┌──┐       PAD2      ║
                  ┌─╨─────────────┐                    └──┘J1         *┌───┐║
                  │               │ ┌┐         *┌───────────┐          │   │║
                  │               │ └┘J2        └───────────┘          │   │║
                  │     CN1       │                      PAD3          │   │║
                  │    [RJ45]     │                                    │   │║
                  │               │                                    │   │║
                  │               │                                    │   │║
                  └─╥─────────────┘*                                   │   │║
                  ┌─╨─────────────┐                                    │   │║
                  │               │ ┌┐                                 │   │║
                  │               │ └┘J4                             J5│   │║
                  │     CN2       │                                    │   │║
                  │    [RJ45]     │                                    │   │║
                  │               │                                    │   │║
                  │               │                                    │   │║
                  └─╥─────────────┘*                                   │   │║
                    ║                                                  │   │║
                    ║┌───────┐*                                        │   │║
                    ║│       │                                         │   │║
                    ║│  T1   │                                         │   │║
                    ║│       │                                         │   │║
                    ║└───────┘                                         └───┘║
                    ║         BAT1                SWD            RST1       ║
                    ║  ()    *┌───┐              *┌───────────┐ *┌───┐  ()  ║
                    ║         └───┘               └───────────┘  └───┘      ║
                    ╚═══════════════════════════════════════════════════════╝
```

## Components Overview ##
The diagram below shows how the different components are interconnected. Details about pin-outs can be found on the next section.
```
#!
     ( < > )          
        ║             
        ║┌───────┐           ┌───────────────┐                     ┌──────────┐
        ║│       │           │               │                     │          │
        ╚╡ RFM69 ╞═══ SPI ═══╡               │      (serial)       │          │
         │       │           │      MCU      │<------ TX/RX ------>│   RPI    │
         └───────┘           │    [STM32]    │<------ RX/TX ------>│          │           ┌────────┐
         ┌───────┐           │               │                     │          ╞═══ I²C ═══╡ EEPROM │
         │ FLASH ╞═══ SPI ═══╡               │                     │          │           └────────┘
         └───────┘           └──────┬┬───────┘                     └────┬┬────┘
                                    ││                                  ││
                                    ││                                  ││
                                ┌───┴┴───┐                          ┌───┴┴───┐
                                │        │<--------- CAN_H -------->│        │
                                │  CAN   │                          │  CAN   │
                                │        │<--------- CAN_L -------->│        │
                                └────────┘                          └────────┘
```

## Pinout ##
The Talk² RPI Hat has a few headers and connectors. Find below the pin-out and details about each one.

### Antenna - CN3 ###
The 50Ω SMA Female connector is attached to the RFM69 radio module. To prevent any damage to the RF module, make sure there's always an antenna connected when transmitting.

To improve the range a bigger or directional antenna might be used. Remember to keep the cable as short as possible to minimize signal lost.

### Main Header - J5 ###
The main header is the 40 pin interface between the RPI and the HAT. Via this header the RPI can interact with a the following components on the HAT:

* STM32F103 MCU: UART, Boot0 and Reset
* MCP2551 CAN Interface: SPI and Reset
* CAT24C32 I²C EEPROM: I²C and Write-Protect

```
#!
                                                     *┌──────┐
                                       3V3:PIN_01 ────│ Θ  Θ │──── PIN_02:5V
                           I2C_SDA:GPIO_02:PIN_03 ────│ Θ  Θ │──── PIN_04:5V
                           I2C_SDL:GPIO_03:PIN_05 ────│ Θ  Θ │──── PIN_06:GND
                                   GPIO_04:PIN_07 ────│ Θ  Θ │──── PIN_08:GPIO_14:UART_TXD    [MCU_UART1_RX:PA10]
                                       GND:PIN_09 ────│ Θ  Θ │──── PIN_10:GPIO_15:UART_RXD    [MCU_UART1_TX:PA09]
                                   GPIO_17:PIN_11 ────│ Θ  Θ │──── PIN_12:GPIO_18
           [MCU_Boot0]             GPIO_27:PIN_13 ────│ Θ  Θ │──── PIN_14:GND
                                   GPIO_22:PIN_15 ────│ Θ  Θ │──── PIN_16:GPIO_23
                                       3V3:PIN_17 ────│ Θ  Θ │──── PIN_18:GPIO_24
        [CAN_SPI_MOSI]    SPI_MOSI:GPIO_10:PIN_19 ────│ Θ  Θ │──── PIN_20:GND
        [CAN_SPI_MISO]    SPI_MISO:GPIO_09:PIN_21 ────│ Θ  Θ │──── PIN_22:GPIO_25             [CAN_SPI_INT]
         [CAN_SPI_CLK]     SPI_CLK:GPIO_11:PIN_23 ────│ Θ  Θ │──── PIN_24:GPIO_08:SPI_CE0     [CAN_SPI_CS]
                                       GND:PIN_25 ────│ Θ  Θ │──── PIN_26:GPIO_07:SPI_CE0
      [EEPROM_I2C_SDA]               ID_SD:PIN_27 ────│ Θ  Θ │──── PIN_28:ID_SC               [EEPROM_I2C_SCL]
                                   GPIO_05:PIN_29 ────│ Θ  Θ │──── PIN_30:GND
                                   GPIO_06:PIN_31 ────│ Θ  Θ │──── PIN_32:GPIO_12
                                   GPIO_13:PIN_33 ────│ Θ  Θ │──── PIN_34:GND
                                   GPIO_19:PIN_35 ────│ Θ  Θ │──── PIN_36:GPIO_16             [EEPROM_Write-Protect]
           [CAN_Reset]             GPIO_26:PIN_37 ────│ Θ  Θ │──── PIN_38:GPIO_20
                                       GND:PIN_39 ────│ Θ  Θ │──── PIN_40:GPIO_21             [MCU_Reset]
                                                      └──────┘
```
On the drawing above identifies the pins used by the Talk² RPI HAT and its function between square brackets, for example [MCU_Reset]. Make sure there's no pin conflict in case additional hardware is connected to the RPI Pins when using this HAT.

Many operations can be done by simply changing the GPIO levels on the RPI, for example, to reset the MCU is just a matter of OUTPUT LOW on the GPIO_21 and change the OUTPUT HIGH again. Additional details about each pin and its usage will be explained along the documentation.

### Terminal Block - T1 ###
The two pin Terminal Block is used to feed power directly into the Talk² RPI HAT. Details about the power supply can be found at the **Power** chapter of this document.

```
#!
                         │     CN2
                         │    [RJ45]    │ 
                         │              │
                         └─╥────────────┘*
                           ║
                           ║┌───────┐*
   (+6V to +30V) 1:VIN ────║│  (x)  │
                           ║│       │ <---- T1
                 2:GND ────║│  (x)  │
                           ║└───────┘
                           ║
                           ║   ( ) <---- Mounting Hole
                           ║
                           ╚══════════
```

### RJ45 - CN1, CN2 ###
Although it's very common to use RJ45 for Ethernet network, that's not the case here. Both RJ45 are connected to the same traces, working as a stub. This has been done simplify connections when creating a CANBus daisy chain.

Apart from data lines, in this case, Can-Low and Can-High, the VIN and Ground are also available on the RJ45. This has been designed to share a single power supply for the whole network/bus.

Each RJ45 has 2 built-in LEDs. Apart from the LED1, which will light-up when the HAT is powered, theother three LEDs are connected to the MCU GPIOs through a resistor according to the drawing below. The LEDs can be used for any propose, including blinking during CANBus traffic or any indicating error.

```
#!
                   CN1                CN2             T1
            ┌────────────────┐ ┌────────────────┐  ┌───────┐

             LED1        LED2   LED3        LED4
             5V_OK    MCU_PB9   MCU_PA5  MCU_PA6
              ┌────────────┐*    ┌────────────┐*     +   -
              │█┌───══───┐█│     │█┌───══───┐█│    *┌─────┐
              │ │        │ │     │ │        │ │     ├──┬──┤
              │ │87654321│ │     │ │87654321│ │     │╔╗│╔╗│
              │ └┴┴┴┴┴┴┴┴┘ │     │ └┴┴┴┴┴┴┴┴┘ │     │╚╝│╚╝│
            ▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀
                 ││││││││           ││││││││         │   │
                 │││││││└ 1:GND     │││││││└ 1:GND   │   └ 2:GND
                 ││││││└─ 2:GND     ││││││└─ 2:GND   └──── 1:VIN
                 │││││└── 3:NC      │││││└── 3:NC
                 ││││└─── 4:CAN_H   ││││└─── 4:CAN_H
                 │││└──── 5:CAN_L   │││└──── 5:CAN_L
                 ││└───── 6:NC      ││└───── 6:NC
                 │└────── 7:VIN     │└────── 7:VIN
                 └─────── 8:VIN     └─────── 8:VIN
```
***Attention: Don't try to connect a Network Cable on the Talk² RPI Hat or some magic smoke might appear.***

### BAT1, SWD and RST1 ###
Those headers are used to interface with the STM32 MCU.
```
#!
         BAT1                   SWD                    RST1 
       *┌─────┐               *┌───────────┐         *┌─────┐
        │ Θ Θ │                │ Θ Θ Θ Θ Θ │          │ Θ Θ │
        └─────┘                └───────────┘          └─────┘
          │ │                    │ │ │ │ │              │ │
          │ └ 2:MCU_VBAT         │ │ │ │ └ 5:MCU_NRST   │ └ 2:GND
          └── 1:GND              │ │ │ └── 4:MCU_SWDIO  └── 1:MCU_NRST
                                 │ │ └──── 3:GND
                                 │ └────── 2:MCU_SWCLK
                                 └──────── 1:3V3R1
```
#### SWD ####
The SWD or Serial Wire Debugging can be used to program and debug the MCU when connected to a ST-Link programmer.

Note that the 3V3R1 is the Target voltage, in other words, the voltage used by the STM32F103 MCU on the HAT. Do not feed any current through this header.

#### RST1 ####
The RST1 can be used to reset the MCU. Shorting RST1 will pull the MCU_NRST low, resetting the MCU.

The same MCU_NRST line is connected to the RPI GPIO_21, making possible to reset the MCU from the RPI via software. This header should be used only in case external control of the MCU reset is required.

#### BAT1 ####
The BAT1 positive contact is connected to the MCU_VBAT.

The BAT1 can be used to power the STM32F103 Real-Time Clock and backup registers when there's no power to the board. The supplied voltage must between 1.8V and 3.6V, normally delivered from a coin-cell like CR2032.

### PAD1, PAD2 AND PAD3 ###
Those are extension PADs and can be used to access STM32F103 pins via PAD1 and PAD2 as well a few RPI pins via PAD3.
```
#!
         PAD1                                         PAD2
       *┌───────────┐                               *┌───────────┐
        │ Θ Θ Θ Θ Θ │                                │ Θ Θ Θ Θ Θ │
        └───────────┘                                └───────────┘
          │ │ │ │ │                                    │ │ │ │ │
          │ │ │ │ └ 5:MCU_PB11                         │ │ │ │ └ 5:3V3R2
          │ │ │ └── 4:MCU_PB10                         │ │ │ └── 4:MCU_PB12
          │ │ └──── 3:MCU_PA3                          │ │ └──── 3:MCU_PB13
          │ └────── 2:MCU_PA2        PAD3              │ └────── 2:MCU_PB14
          └──────── 1:GND          *┌───────────┐      └──────── 1:MCU_PB15
                                    │ Θ Θ Θ Θ Θ │
                                    └───────────┘
                                      │ │ │ │ │
                                      │ │ │ │ └ 5:RPI_5V
                                      │ │ │ └── 4:RPI_I2C_SDA:GPIO_02:PIN_03
                                      │ │ └──── 3:RPI_I2C_SCL:GPIO_03:PIN_05
                                      │ └────── 2:GND
                                      └──────── 1:RPI_3V3
```
#### PAD1 and PAD2 ####
Instead of leave a MCU pins disconnected, the Talk² RPI HAT expose those pins so users can take advantage and extend the HAT functionalities. The PAD1 and PAD2 pins have been carefully chosen to allow a many peripherals as possible, including USART, ADC, TIMERS, I²C and SPI.

#### PAD3 ####
Although the RPI pins still can be accessed via the main 40 pin header, the HAT provides direct access to the RPI I²C and power pins. This has been designed to accommodate an OLED screen and display messages from the RPI.

## Power ##
The Talk² RPI HAT can be powered by the Raspberry PI 5V via the Main Header. The HAT also counts with a Microchip MCP16311 1A step-down switching regulator. The regulator is cable of powering the HAT and back-power the RPI at same time, just be aware it's limited to 1A. For that reason connecting external USB devices to the RPI, like hard-disks, is not recommended in this configuration.

To power the systems using the Talk² RPI HAT, just connect a power supply between 6V and 30V to the Terminal Block. The same can be archived by using the RJ45 connectors, which are CANBus + Power.

TO DO: Provide a table with different supplied voltages and minimum current required.
TO DO: Provide a table showing recommended voltage and current for different cable lengths.

If there are multiple devices chained together and powered over a CAT5, remember to use the highest voltage possible. This will reduce the current and minimize any voltage drop along the cable. A 18V or 24V power supply is recommended in this case.

*Although it shouldn't be a problem, there's no need to connect the RPI and the HAT power supplies at the same time.*

The Talk² RPI HAT also has a Dual LDO regulator to lower the voltage from 5V to 3.3V as many components need lower voltage. The LDO outputs rails are identified as 3V3R1 and 3V3R2.

* 3V3R1 is being used by the board components which require 3.3 volts;
* 3V3R2 is available to the user on PAD2.

Note that the RPI and Talk² RPI Hat 5V rails are connected together. The RPI 3.3V rail remains isolated, with a single special case: the CANBus components used by the RPI, CAN Interface MCP2515 and the CAN Transceiver, are powered by the RPI 3.3 volts and 5 volts, from the RPI 40 pin header.

To prevent permanent damage to the boards, make sure the correct voltage is supplied, remember that RPI and many components of this HAT do not tolerate 5V.

### Voltage Monitor ###
The Talk² RPI HAT counts with a resistor divider (RTop:1M RBot:52K3) attached to the VIN, which is connected to the MCU_PA4 pin, allowing the VIN bus voltage to be monitored by the ADC.

## CANBus ##
The board counts with 2 CAN 2.0B Interfaces, one from the STM32 MCU and another from the MCP2515, which is connected to the RPI via the SPI lines. Both CAN interfaces are connected to high-speed CAN Transceivers. The transceivers were specially selected to support 3.3V logic.

### STM32 CAN ###
The STM32F103 has a built-in CANBus interface and it's connected to the CAN Transceiver using the following MCU Pins:

  * CAN_RX: MCU_PB08
  * CAN_TX: MCU_PB09

### RPI CAN ###
The CANBus Interface is a MCP2515 and is clocked by a dedicated 16MHz crystal. The MCP2515 is connected to the RPI via SPI lines and all the GPIO/Pins details can be found in the "Pinout" section of this document.

Note that it's possible to control the "Reset" state of the MCP2515 CAN Interface via the RPI GPIO_26:PIN_37.

### Physical Connection ###
Note that both RJ45 connectors are attached to the same lines/traces. The CANBus data lines, CAN_High and CAN_Low are available on the RJ45's pins 4:CAN_H and 5:CAN_L. Also, the CANBus is already terminated with two 120 Ohms resistor, which can be independently disconnected by removing J2 and/or J4. See the example below:

```
#!
            ┌───────────────────┐        ┌───────────────────┐        ┌───────────────────┐
            │      Node A       │        │      Node B       │        │      Node C       │
            │                   │        │                   │        │                   │
            │ CAN1 █══╦══█ CAN2 │        │ CAN1 █══╦══█ CAN2 │        │ CAN1 █══╦══█ CAN2 │
            │         ║         │        │         ║         │        │         ║         │
            │  T   ┌─┐║┌─┐   T  │        │  T   ┌─┐║┌─┐   T  │        │  T   ┌─┐║┌─┐   T  │
            │ [X]══╪═╪╩╪╦╪══[ ] │        │ [ ]══╪╦╪╩╪╦╪══[ ] │        │ [ ]══╪╦╪╩╪═╪══[X] │
            │      └─┘ └╫┘      │        │      └╫┘ └╫┘      │        │      └╫┘ └─┘      │
            └───────────╫───────┘        └───────╫───╫───────┘        └───────╫───────────┘
                        ║                        ║   ║                        ║
                        ╚════════════════════════╝   ╚════════════════════════╝
```
On the network above the J2 has been removed from Nodes A and B and the J4 has been removed from nodes B and C. The idea is to leave the terminator resistors only on the furthest nodes. For the Talk² RPI HAT each node is actually made of two CAN devices as each board contains 2 transceivers.

When connecting the Talk² RPI HAT to an existent CAN Bus daisy chain, like a Car, make sure both the terminators are disconnected.

## MCU ##
Information about MCU pinout, peripherals, crystal, etc

## RFM69 ##
How RFM69 is wired

# Software #
The information presented on this section will be based on a Raspberry PI running Raspbian OS. If you use a different operation system you might need to adapt the instructions accordingly.

The Raspberry Pi doesn't have a BIOS like you'd find on a conventional PC. For that reason some configuration changes are required to be done on the `/boot/config.txt` file to enable some special drivers. On the next sections you might see configuration changes that need to be done to this file and they only take effect after the RPI is restarted.

More information about boot/config.txt can be found here: https://www.raspberrypi.org/documentation/configuration/config-txt.md

## CANBus ##

### Configuration and Modules ###
To enable the CAN Bus controller from the Talk² Talking HAT, you fist need to update your `boot/config.txt` file, adding the following lines to the end:

```
# CANBus MCP251X
dtoverlay=mcp2515-can0-overlay,oscillator=16000000,interrupt=25
dtoverlay=spi-bcm2835-overlay
```

Once the configuration is in place you'll need to restart your RPI.

After that, you will need to configure the CAN Bus network interface. To do that, you need to first "enable" the CAN Bus controller, by changing the MCP2515 "reset" pin state. Later you need to load the MCP2515 modules and bring up the `can0` network interface.

All the steps above might sound complicated, but you can simply use the scripts available on this repository and run:

```
$ cd raspberry-pi-hat/scripts/
$ sudo ./Talk2HAT_GPIO_Control.sh

### Enabling GPIOs used to interact with STM32 MCU ###
  MCU Boot 0 - GPIO27/PIN13: out: 0
  RPI CAN Reset - GPIO26/PIN37: out: 1
  MCU Reset - GPIO21/PIN40: out: 1
  EEPROM R/W - GPIO16/PIN36: out: 0

$ sudo ./Talk2HAT_ConfigureCan.sh
If this command fails, make sure CAN Reset GPIO is in the correct state.

### Removing mcp251x module ###
### Loading mcp251x module ###
### Configuring CAN0 interface and showing Stats ###
3: can0: <NOARP,UP,LOWER_UP,ECHO> mtu 16 qdisc pfifo_fast state UNKNOWN mode DEFAULT qlen 1000
    link/can
    can state ERROR-ACTIVE restart-ms 500
    bitrate 250000 sample-point 0.875
    tq 250 prop-seg 6 phase-seg1 7 phase-seg2 2 sjw 1
    mcp251x: tseg1 3..16 tseg2 2..8 sjw 1..4 brp 1..64 brp-inc 1
    clock 8000000
    re-started bus-errors arbit-lost error-warn error-pass bus-off
    0          0          0          0          0          0
    RX: bytes  packets  errors  dropped overrun mcast
    2          1        0       1       0       0
    TX: bytes  packets  errors  dropped carrier collsns
    0          0        0       0       0       0

```

Now you should be able to see the `can0` interface:

```
$ ifconfig can0
can0      Link encap:UNSPEC  HWaddr 00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00
          UP RUNNING NOARP  MTU:16  Metric:1
          RX packets:1 errors:0 dropped:1 overruns:0 frame:0
          TX packets:0 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:1000
          RX bytes:2 (2.0 B)  TX bytes:0 (0.0 B)

```

See on the next section how you can test your CAN Bus interface using `cansend` and `candump`.

### Tools ###
candump, cansend, canplay

#### Python-can ####
Basic Python script to interact with CANBus

## MCU ##

### Serial/UART ###
The STM32 MCU present on the Talking HAT is connected to the RPI UART/Serial TX and RX pins. This allows a "console" access to the MCU in case your STM32 application implements it.

By default the Raspian has the Serial port attached to a TTY, or Linux Console. This allows access to the Raspian console without having to connect a Keyboard and Monitor. Well, normally the RPI will be connected to the network as well and access via SSH is commonly used.

To communicate from the RPI to the STM32 MCU via serial, first you'll need to disable the Serial TTY mentioned above. To do that, edit the file `/etc/inittab` and comment one line as showed below:

```
sudo vi /etc/inittab

(...)

#Spawn a getty on Raspberry Pi serial line
#T0:23:respawn:/sbin/getty -L ttyAMA0 115200 vt100
(...)
```

Commenting the line `T0:23:respawn:/sbin/getty -L ttyAMA0 115200 vt100` and restarting the RPI will have free the Serial Port. Now you can use `minicom`, or any other Terminal, to connect to the `/dev/ttyAMA0`.

### Firmware ###
Describe how to perform a firmware upgrade via Serial/UART and BOOT0 GPIO

### Hardware Reset ###
How to send a MCU reset via GPIO